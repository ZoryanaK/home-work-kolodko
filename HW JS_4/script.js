/*
Task 1
Напишіть функцію map(fn, array), яка приймає на вхід функцію та масив, 
та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.
*/


let allNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
function checkingNumbers(numbers) {
    var newArray = [];
    for (i = 0; i < numbers.length; i++) {
        if (numbers[i] % 2 === 1) {
        newArray.push(numbers[i]);}
    }
document.write("Новий масив, що складається тільки з непарних чисел:" + "<br/>" + newArray)
   }

function map(fn, array) {
    return fn(array);
    
}
map (checkingNumbers, allNumbers);

/*
 Task 2
Перепишіть функцію за допомогою оператора '?' або '||'
Наступна функція повертає true, якщо параметр age більше 18. В іншому випадку вона ставить запитання confirm і повертає його результат.
function checkAge(age) {
if (age > 18) {
return true;
} else {
return confirm('Батьки дозволили?');
} }
*/


let old = parseInt(prompt("Введіть свій вік"));
function checkAge(age) {
return (age > 18) ? true : confirm('Батьки дозволили?');
}
checkAge(old)