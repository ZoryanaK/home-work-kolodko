
// Прямоугольник по заданным сторонам

var width = parseFloat(prompt("Введите длину прямоугольника"))
var height = parseFloat(prompt("Введите высоту прямоугольника"))
document.write("1. Прямоугольник с заданой длиной ", width, " и шириной ", height, "<br/>", "<br/>");
for (let i = 0; i < height; i++) {
    if ((i == 0) || (i == (height - 1))) {
        for (let j = 0; j < width; j++) {
            document.write("<div>*</div>");
        }
    }
    if ((i < (height - 1)) && (i != 0)) {
        for (let j = 0; j < width; j++) {
            if ((j == 0) || (j == (width - 1))) {
                document.write("<div>*</div>");
            }
            else {
                document.write("<div>&nbsp;&nbsp;</div>");
            }
        }
    }
    document.write("<br/>");
}
document.write("<br/>");

// Прямоугольный треугольник по заданным сторонам

var triangle = parseFloat(prompt("Введите сторону для прямоугольного треугольника, для равнобедренного треугольника и для ромба"))
document.write("2. Прямоугольный треугольник с заданой стороной ", triangle, "<br/>", "<br/>");
for (let i = 0; i < triangle; i++) {
    if ((i == 0) || (i == (triangle - 1))) {
        for (let j = i; j < triangle; j++) {
            document.write("<div>*</div>");
        }
    }
    if ((i < (triangle - 1)) && (i != 0)) {
        for (let j = i; j < triangle; j++) {
            if ((j == i) || (j == (triangle - 1))) {
                document.write("<div>*</div>");
            }
            else {
                document.write("<div>&nbsp;&nbsp;</div>");
            }
        }
    }
    document.write("<br/>");
}

// Равнобедренный треугольник по заданным сторонам

document.write("3. Равнобедренный треугольник с заданой стороной ", triangle, "<br/>", "<br/>");
var triangleTwo = (triangle * 2)

for (let i = 0; i < (triangleTwo / 2); i++) {
    if (i == 0) {
        for (let j = 0; j < triangleTwo; j++) {
            if (j < (triangleTwo - 1)) {
                document.write("<div>*</div>");
            }
            else {
                document.write("<div>&nbsp;&nbsp;</div>");
            }
        }
    }

    else {
        for (let j = 0; j < triangleTwo; j++) {
            if ((j == i) || (j == (triangleTwo - i - 2))) {
                document.write("<div>*</div>");
            }
            else {
                document.write("<div>&nbsp;&nbsp;</div>");
            }
        }
    }
    document.write("<br/>")
}
document.write("<br/>");

// Ромб по заданным сторонам

document.write("4. Ромб с заданной стороной ", triangle, "<br/>", "<br/>");
for (let i = 0; i < ((triangleTwo / 2) - 1); i++) {
    for (let j = 0; j < triangleTwo; j++) {
        if ((j == (triangle + i - 1)) || (j == (triangle - i - 1))) {
            document.write("<div>*</div>");
        }
        else {
            document.write("<div>&nbsp;&nbsp;</div>");
        }
    }

    document.write("<br/>")
}
for (let i = 0; i < (triangleTwo / 2); i++) {
    for (let j = 0; j < triangleTwo; j++) {
        if ((j == i) || (j == (triangleTwo - i - 2))) {
            document.write("<div>*</div>");
        }
        else {
            document.write("<div>&nbsp;&nbsp;</div>");
        }
    }
    document.write("<br/>")
}


// Сумма значений в заданом промежутке

var figureA = parseFloat(prompt("Введите меньшее число А"))
var figureB = parseFloat(prompt("Введите большее число В"))
document.write("5.1. Нечетные числа в промежутке от А ", figureA, " до В ", figureB, " :", "<br/>");
var res = 0
while (figureA <= figureB) {
    if (figureA % 2 == 1) {
        document.write(figureA, "<br/>")
    }
    res += figureA
    figureA++
}
document.write("<br/>", "5.2. Сумма всех чисел в промежутке от А до В составляет:", "<br/>", res, "<br/>", "<br/>");


// Переписать код через ?
document.write("6. Переписать код через ? :", "<br/>");

var a = 1
var b = 2
a + b < 4 ?
    document.write("Мало", "<br/>", "<br/>") : document.write("Много", "<br/>", "<br/>");

let login = "Директор"
let message = (login == "Вася") ? "Hello" : (login == "Директор") ? "Здравствуйте" : (login == "") ? "Нет логина" : "Не правильный логин";
document.write(message);
