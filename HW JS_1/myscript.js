// Task 1
let price = prompt('Введіть ціну товару');
let amountFirst = price*10*0.03;
let amountSecond = price*20*0.05;

console.log("Знижка 3% при покупці 10шт складе: ", amountFirst, " грн");
console.log("Знижка 5% при покупці 20шт складе: ", amountSecond, " грн");

// Task 2
let sizeOne = prompt('Введіть першу сторону прямокутника в см');
let sizeTwo = prompt('Введіть другу сторону прямокутника в см');
let square = sizeOne * sizeTwo;

console.log("Площа прямокутника: ", square, " см");
