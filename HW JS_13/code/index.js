const pizza = {
    size: 85,
    sauce: 0,
    topping: 0,
}

const price = {
    size: {
        small: 50,
        mid: 75,
        big: 85,
    },
    sauce: {
        sauceClassic: 20,
        sauceBBQ: 30,
        sauceRikotta: 30,
    },
    topping: {
        moc1: 30,
        moc2: 30,
        moc3: 30,
        telya: 40,
        vetch1: 40,
        vetch2: 40,
    }
}

function $(selector) {
    return document.querySelector(selector);
}

window.addEventListener("DOMContentLoaded", () => {
    // Шукаємо розмір коржу
    document.getElementById("pizza")
        .addEventListener("click", function (ev) {
            //console.log(ev.target.id);
            //Визначаємо обраний корж і записуємо данні в обьєкт 
            switch (ev.target.id) {
                case "small": pizza.size = price.size.small;
                    break
                case "mid": pizza.size = price.size.mid;
                    break
                case "big": pizza.size = price.size.big;
                    break
            }
            show(pizza)
        })
    show(pizza)
    btnRun()

    //метод для виводу інформації про продукт
    function show(pizza) {
        //отримали блок ціни
        const price = document.querySelector("#price");
        res = parseFloat(pizza.size + pizza.sauce + pizza.topping)
        price.innerText = res
    }

    function btnRun() {

        const btn = document.querySelector("#banner");

        btn.addEventListener("mousemove", () => {
            const coords = {
                X: Math.floor(Math.random() * document.body.clientWidth),
                Y: Math.floor(Math.random() * document.body.clientHeight)
            }
            if ((coords.X + 350) > document.body.clientWidth) {
                return
            }
            if ((coords.Y + 150) > document.body.clientHeight) {
                return
            }
            btn.style.top = coords.Y + "px"
            btn.style.left = coords.X + "px"
        })

        const source = document.querySelectorAll('.ingridients img');
        const dragStart = function (e) {
            e.dataTransfer.setData("id", this.id);
        }
        const dragEnd = function (evt) {
        }

        source.forEach((elem) => {
            elem.addEventListener('dragstart', dragStart);
            elem.addEventListener('dragend', dragEnd);
        });

        var target = document.getElementById("target");
        target.addEventListener("dragenter", function (evt) { }, false);
        target.addEventListener("dragleave", function (evt) { }, false);

        target.addEventListener("dragover", function (evt) {
            if (evt.preventDefault) evt.preventDefault();
            return false;
        }, false);

        target.addEventListener("drop", function (evt) {
            if (evt.preventDefault) evt.preventDefault();
            if (evt.stopPropagation) evt.stopPropagation();
            this.style.border = "";

            var id = evt.dataTransfer.getData("id");
            var elem = document.getElementById(id);
            elem.classList.add("disabled");
           
            const img = document.createElement("img");
            img.className = "newElement"
           
            img.setAttribute('src', elem.currentSrc);
            this.appendChild(img);
            elem.parentElement.classList.add("cursorNoDrop");
            if (id.includes("sauce")) {

                switch (id) {
                    case "sauceClassic": pizza.sauce += price.sauce.sauceClassic;
                        break
                    case "sauceBBQ": pizza.sauce += price.sauce.sauceBBQ;
                        break
                    case "sauceRikotta": pizza.sauce += price.sauce.sauceRikotta;
                        break
                }
                var sauceAndBtn = document.createElement("div");
                var saucePlace = document.getElementById("sauce");
                saucePlace.append(sauceAndBtn)
                var sauceName = document.createElement("span");
                var btndelete = document.createElement("button");
                btndelete.innerText = "\u274C"

                sauceName.innerHTML = `<br> ${document.getElementById(id).nextElementSibling.innerText}`;
                sauceAndBtn.append(sauceName)
                sauceName.after(btndelete)
                btndelete.addEventListener("click", () => {
                    sauceAndBtn.remove()
                    switch (id) {
                        case "sauceClassic": pizza.sauce -= price.sauce.sauceClassic;
                            break
                        case "sauceBBQ": pizza.sauce -= price.sauce.sauceBBQ;
                            break
                        case "sauceRikotta": pizza.sauce -= price.sauce.sauceRikotta;
                            break
                    }
                    show(pizza)
                    img.remove()
                    elem.classList.remove("disabled");
                    elem.parentElement.classList.remove("cursorNoDrop");
                })
            }
            else {
                switch (id) {
                    case "moc1": pizza.topping += price.topping.moc1;
                        break
                    case "moc2": pizza.topping += price.topping.moc2;
                        break
                    case "moc3": pizza.topping += price.topping.moc3;
                        break
                    case "telya": pizza.topping += price.topping.telya;
                        break
                    case "vetch1": pizza.topping += price.topping.vetch1;
                        break
                    case "vetch2": pizza.topping += price.topping.vetch2;
                        break
                }
                var toppingAndBtn = document.createElement("div");
                var toppingPlace = document.getElementById("topping");
                toppingPlace.append(toppingAndBtn)
                var toppingName = document.createElement("span");
                toppingName.innerHTML = `<br> ${document.getElementById(id).nextElementSibling.innerText}`;
                toppingAndBtn.append(toppingName)
                var btndelete = document.createElement("button");
                btndelete.innerText = "\u274C"
                toppingName.after(btndelete)
                show(pizza)

                btndelete.addEventListener("click", () => {
                    toppingAndBtn.remove()
                    switch (id) {
                        case "moc1": pizza.topping -= price.topping.moc1;
                        break
                    case "moc2": pizza.topping -= price.topping.moc2;
                        break
                    case "moc3": pizza.topping -= price.topping.moc3;
                        break
                    case "telya": pizza.topping -= price.topping.telya;
                        break
                    case "vetch1": pizza.topping -= price.topping.vetch1;
                        break
                    case "vetch2": pizza.topping -= price.topping.vetch2;
                        break
                    }
                    show(pizza)
                    img.remove()
                    elem.classList.remove("disabled");
                    elem.parentElement.classList.remove("cursorNoDrop");
                })
            }
            show(pizza)
            return false;
        }, false);
    }

    $("#btnSubmit").addEventListener("click", () => {
        const patternName = /^[Є-яҐ]{2,}$/;
        const patternTel = /^\+380\d{9}$/;
        const patternMail = /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$/gi;
        if (patternName.test($("#name").value) && patternTel.test($("#tel").value) && patternMail.test($("#mail").value)) {
            console.log("fff")
            document.location = "./thank-you/index.html";
        }
        else {
            if (!patternName.test($("#name").value)) { document.getElementById("name").classList.add("redbackground") }
            if (!patternTel.test($("#tel").value)) { document.getElementById("tel").classList.add("redbackground") }
            if (!patternMail.test($("#mail").value)) { document.getElementById("mail").classList.add("redbackground") }
        }
    }
    )
})