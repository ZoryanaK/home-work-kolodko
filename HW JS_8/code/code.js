/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
https://storage.googleapis.com/www.examclouds.com/oop/car-ierarchy.png
*/ 


class Driver {
    constructor (fullName, experiance) {
        this.fullName = fullName
        this.experiance = experiance
    }
}

class Engine extends Driver {
    constructor (fullName, experiance, power, company) {
        super(fullName, experiance)
        this.power = power
        this.company = company
    }
}

class Car extends Engine{
    constructor (fullName, experiance, power, company,brand, carClass) {
        super(fullName, experiance, power, company);
        this.brand = brand;
        this.carClass = carClass;
       
    }
    start() {
        console.log(`Поїхали!`)
    }
    stop() {
        console.log(`Зупиняємося!`)
    }    
    turnRight() {
        console.log(`Поворот праворуч!`)
    }

    turnLeft() {
        console.log(`Поворот ліворуч!`)
    }

    toString() {
        console.log(`Автомобіль марки: ${this.brand} ; клас: ${this.carClass}. Водій: ${this.fullName}; стаж: ${this.experiance} років. Двигун з потужністю: ${this.power}; торгова марка: ${this.company}.`)
    }
}

class Lorry extends Car {
    constructor (fullName, experiance, power, company,brand, carClass, weight) {
        super(fullName, experiance, power, company,brand, carClass)
        this.weight = weight
    }
    showWeight() {
        console.log(`Вантажепід'ємність ${this.weight}`)
    }
}

class SportCar extends Car {
    constructor (fullName, experiance, power, company,brand, carClass, speed) {
        super(fullName, experiance, power, company,brand, carClass)
        this.speed = speed
    }
    showSpeed() {
        console.log(`Гранична швидкість ${this.speed}`)
    }
}

const newCar = new Car ("Petrov Petro Petrovych", 15, "200 ph", "VAG", "BMW", "executive")
newCar.toString()

const newLorry = new Lorry ("Petrov Petro Petrovych", 15, "200 ph", "VAG", "Mercedes", "lorry", "5t")
newLorry.toString()
newLorry.showWeight()

const newSportCar = new SportCar ("Slavnyj Vasyl Stepanovych ", 10, "200 ph", "VAG", "Ford", "sport", "300km/h")
newSportCar.toString()
newSportCar.showSpeed()