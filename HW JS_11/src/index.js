/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/


/*Переменные*/
let show = "0";
let el = document.getElementById("screen");
let numberfirst;
let numbersecond;
let operation;
const figureItis = document.querySelector("#itis");
let memoryFigures = "";


/*Функция вывод на экран*/
function showResults() {
    el.value = show;
}
showResults();

/*Функция логика записи цифр*/
function writeFigures() {
    if (show == "0" && show != "0.") {
        show = "";
    }
    if (numberfirst != undefined && numbersecond != undefined) {
        show = ""
        numbersecond = undefined;
    }


}

/*Функция расчета, кроме =*/
function calculate() {
    if (numberfirst != undefined) {
        if (numberfirst == "division by 0" || show == "division by 0") {
            numberfirst == "0";
            numbersecond == undefined;
            show = "0"
        }
        else {
            numbersecond = show;
            if (operation == "+") {
                numberfirst = parseFloat(numberfirst) + parseFloat(numbersecond);
                show = numberfirst;
            }
            else if (operation == "-") {
                numberfirst = parseFloat(numberfirst) - parseFloat(numbersecond);
                show = numberfirst;
            }
            else if (operation == "*") {
                numberfirst = parseFloat(numberfirst) * parseFloat(numbersecond);
                show = numberfirst;
            }

            else if (operation == "=") {
                numberfirst = parseFloat(numberfirst)
                show = numberfirst;
            }
            else if (operation == "/") {
                if (numbersecond == 0) {
                    show = "division by 0";
                    numberfirst == undefined
                }
                else {
                    numberfirst = parseFloat(numberfirst) / parseFloat(numbersecond);
                    show = numberfirst;
                }
                numbersecond == undefined;
            }
        }
    }
    else {
        figureItis.disabled = false
        numberfirst = show;
        show = "0";
    }
}

/*События*/
window.addEventListener("DOMContentLoaded", (e) => {
    const figureDot = document.querySelector("#dot");
    figureDot.addEventListener("click", (e) => {
       let showArray = Array.from(show);
        if (showArray.includes(".") == true) {
            return
        }
        else {
            if (numberfirst != undefined && numbersecond != undefined) {
                show = "0"
                numbersecond = undefined;
            }
            show += (figureDot.value);
        }
    }
    )

    const figureOne = document.querySelector("#one");
    figureOne.addEventListener("click", (e) => {
        writeFigures();
        show += (figureOne.value);
    })

    const figureTwo = document.querySelector("#two");
    figureTwo.addEventListener("click", (e) => {
        writeFigures();
        show += (figureTwo.value);
    });

    const figureThree = document.querySelector("#three");
    figureThree.addEventListener("click", (e) => {
        writeFigures();
        show += (figureThree.value);
    });

    const figureFour = document.querySelector("#four");
    figureFour.addEventListener("click", (e) => {
        writeFigures();
        show += (figureFour.value);
    });

    const figureFive = document.querySelector("#five");
    figureFive.addEventListener("click", (e) => {
        writeFigures();
        show += (figureFive.value);
    });

    const figureSix = document.querySelector("#six");
    figureSix.addEventListener("click", (e) => {
        writeFigures();
        show += (figureSix.value);
    });

    const figureSeven = document.querySelector("#seven");
    figureSeven.addEventListener("click", (e) => {
        writeFigures();
        show += (figureSeven.value);
    });

    const figureEight = document.querySelector("#eight");
    figureEight.addEventListener("click", (e) => {
        writeFigures();
        show += (figureEight.value);
    });

    const figureNine = document.querySelector("#nine");
    figureNine.addEventListener("click", (e) => {
        writeFigures();
        show += (figureNine.value);
    });

    const figureZero = document.querySelector("#zero");
    figureZero.addEventListener("click", (e) => {
        if (show == "0") {
            return
        }
        else {
            if (numberfirst != undefined && numbersecond != undefined) {
                show = ""
                numbersecond = undefined;
            }
            show += (figureZero.value);
            show = show.slice(0, 17)
        }

    });

    const figurePlus = document.querySelector("#plus");
    figurePlus.addEventListener("click", (e) => {
        calculate();
        operation = "+"
    });

    const figureMinus = document.querySelector("#minus");
    figureMinus.addEventListener("click", (e) => {
        calculate();
        operation = "-"
    });

    const figureDivide = document.querySelector("#divide");
    figureDivide.addEventListener("click", (e) => {
        calculate();
        operation = "/"
    });

    const figureMultipl = document.querySelector("#multipl");
    figureMultipl.addEventListener("click", (e) => {
        calculate();
        operation = "*"
    });

    figureItis.addEventListener("click", (e) => {
        if (numberfirst != undefined) {
            if (numberfirst == "division by 0" || show == "division by 0") {
                numberfirst == "0";
                numbersecond == undefined;
                show = "0"
            }
            else {
                numbersecond = show;
                if (operation == "+") {
                    numberfirst = parseFloat(numberfirst) + parseFloat(numbersecond);
                    show = numberfirst;
                }
                else if (operation == "-") {
                    numberfirst = parseFloat(numberfirst) - parseFloat(numbersecond);
                    show = numberfirst;
                }
                else if (operation == "*") {
                    numberfirst = parseFloat(numberfirst) * parseFloat(numbersecond);
                    show = numberfirst;
                }
                else {
                    if (numbersecond == 0) {
                        show = "division by 0"
                        numberfirst == undefined
                    }
                    else {
                        numberfirst = parseFloat(numberfirst) / parseFloat(numbersecond);
                        show = numberfirst;
                    }
                    numbersecond == undefined;
                }
            }

        }
        else {
            numberfirst = show;
            show = "0";
        }
        operation = "="
    });

    const buttonDelete = document.querySelector("#buttonDelete");
    buttonDelete.addEventListener("click", (e) => {
        if (numberfirst != undefined && numbersecond != undefined) {
            numberfirst = undefined
            numbersecond = undefined
            show = "0"
        }
        else {
            show = show.slice(0, -1);
        }
    });

    /*Память*/
    memoryPlus = document.querySelector("#memoryPlus");
    memoryPlus.addEventListener("click", (e) => {
        memoryFigures = show;
        document.getElementById("memory").innerHTML = "Память: " + memoryFigures;
    });

    memoryMin = document.querySelector("#memoryMin");
    memoryMin.addEventListener("click", (e) => {
        memoryFigures = 0;
        document.getElementById("memory").innerHTML = "Память: " + memoryFigures;
    });

    memoryReturn = document.querySelector("#memoryReturn");
    memoryReturn.addEventListener("click", (e) => {
        show = memoryFigures;
        document.getElementById("memory").innerHTML = "Память: " + memoryFigures;
    });

    document.querySelector(".box").addEventListener("click", (e) => {
        showResults()
    });

}, false);
