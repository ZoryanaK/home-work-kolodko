/*- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, 
в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

window.addEventListener("DOMContentLoaded", (e) => {
    const header = document.createElement("p");
    header.innerHTML = "Price"
    task.after(header);
    const input = document.createElement("input");
    input.type = "number"
    header.after(input);
    const span = document.createElement("span");
    task.append(span)
    const spanWrongPrice = document.createElement("span")
    input.after(spanWrongPrice)
    const newLine = document.createElement("br")
    span.after(newLine)

    input.addEventListener("focus", () =>  {
        input.className = ""
        input.classList.add("focusClass");
    })
    input.addEventListener('blur', () => {
        input.classList.remove("focusClass");
    });
    input.addEventListener("change", (e) => {
        input.className = ""
        if (input.value < 0) {
            input.className = ""
            input.classList.add("redBorder")
            spanWrongPrice.innerHTML = `Please enter correct price`
            spanWrongPrice.classList.add("wrongPrice")
        }
        else if (input.value > 0) {
            span.innerHTML = ` Price is ${input.value} UAH`;
            button.className = "";
            button.classList.add("buttonVisible");
            input.classList.add("greenBackground");
            button.addEventListener("click", (e) => {
                button.className = "";
                button.classList.add("buttonUnvisible");
                span.innerHTML = "";
            })
            spanWrongPrice.innerHTML = "";
        }
        else if (input.value == 0) {
            input.className = ""
            input.classList.add("redBorder")
            spanWrongPrice.innerHTML = "Enter price"
            spanWrongPrice.classList.add("wrongPrice")
        }
    })
})