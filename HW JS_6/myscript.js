
/* 
Завдання 1. 
Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.*/


function Worker(name, age, rate, days) {
    this.name = name;
    this.age = age;
    this.rate = rate;
    this.days = days;
}

Worker.prototype.getSalary = function () {
    return this.rate * this.days;
}

var workerOne = new Worker("Ivan", 40, 1000, 20);
document.write(`Завдання 1. <br> Зарплата співробітника ${workerOne.name} склала ${workerOne.getSalary()} грн <br> <hr> <br>`)

/* Завданн 2.
Реалізуйте клас MyString, який матиме такі методи: 
1. метод reverse(),який параметром приймає рядок, а повертає її в перевернутому вигляді, 
2. метод ucFirst(), який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
3. та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.*/

function MyString(text) {
    this.text = text;
    this.reverse = function () {
        return text.split('').reverse().join('');
    }
    this.uсFirst = function () {
        return (text.slice(0, 1).toUpperCase() + text.slice(1));
    }
    this.uсWords = function () {
        return text.split(/\s+/).map(word => word[0].toUpperCase() + word.substring(1)).join(' ')
    }
}

var exampleOne = new MyString("hello world")
document.write(`Завдання 2. <br> Введений текст: ${exampleOne.text} <br> Метод revers: ${exampleOne.reverse()} <br>`)
document.write(`Метод uсFirst: ${exampleOne.uсFirst()} <br>`)
document.write(`Метод uсWords: ${exampleOne.uсWords()}`)

